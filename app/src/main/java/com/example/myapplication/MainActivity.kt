package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {

        var text = findViewById<TextView>(R.id.text)
        var et: EditText = findViewById<EditText>(R.id.editText)
        var btn = findViewById<Button>(R.id.button)

        var strfirst = et.text.toString()


        btn.setOnClickListener{
            strfirst = et.text.toString()
            if (strfirst != "")
                text.text = numToWord(strfirst.toInt())
        }

    }

    fun numToWord(g: Int): String {
        val len = g.toString().length
        var current = ""
        if (len == 0) {
            return "ცარიელია"
        }
        if (g === 0) {
            return "ნოლი"
        }

        if (len > 3 && g == 1000) {
            return "ათასი"
        } else if (len > 3) {
            return "აღნიშნული ციფრი მეტია ათასზე"
        }

        val tenths = arrayOf(
            "ოცი",
            "ოცდაათი",
            "ორმოცი",
            "ორმოცდაათი",
            "სამოცი",
            "სამოცდაათი",
            "ოთხმოცი",
            "ოთხმოცდაათი"
        )
        val doubleDig = arrayOf(
            "ათი", "თერთმეტი", "თორმეტი", "ცამეტი", "თოთხმეტი", "თხუთმეტი",
            "თექვსმეტი", "ჩვიდმეტი", "თვრამეტი", "ცხრამეტი", "ოცი"
        )

        val tensMult = arrayOf(
            "", "ათი", "ოცდა", "ოცდა", "ორმოცდა", "ორმოცდა", "სამოცდა", "სამოცდა", "ოთხმოცდა",
            "ოთხმოცდა"
        )

        val numNames = arrayOf(
            "", "ერთი", "ორი", "სამი", "ოთხი", "ხუთი", "ექვსი", "შვიდი", "რვა", "ცხრა",
            "ათი", "თერთმეტი", "თორმეტი", "ცამეტი", "თოთხმეტი", "თხუთმეტი",
            "თექვსმეტი", "ჩვიდმეტი", "თვრამეტი", "ცხრამეტი"
        )

        var number = g
        if (number % 100 < 20) {
            current = numNames[number % 100]
            number /= 100
        } else {
            if ((number / 10) % 2 != 0) {
                current = doubleDig[number % 10]
                number /= 10
            } else {
                current = numNames[number % 10]
                number /= 10
            }

            current = tensMult[number % 10] + current;
            number /= 10;


            when {
                g % 10 == 0 && g / 10 == 2 -> current = tenths[0]
                g % 10 == 0 && g / 10 == 3 -> current = tenths[1]
                g % 10 == 0 && g / 10 == 4 -> current = tenths[2]
                g % 10 == 0 && g / 10 == 5 -> current = tenths[3]
                g % 10 == 0 && g / 10 == 6 -> current = tenths[4]
                g % 10 == 0 && g / 10 == 7 -> current = tenths[5]
                g % 10 == 0 && g / 10 == 8 -> current = tenths[6]
                g % 10 == 0 && g / 10 == 9 -> current = tenths[7]
            }

        }

        if (number == 0) return current;


        when {
            g > 100 && g <= 199 -> return "ას" + current
            g == 100 -> return "ასი"
            g > 200 && g <= 299 -> return "ორას" + current
            g == 200 -> return "ორასი"
            g > 300 && g <= 399 -> return "სამას" + current
            g == 300 -> return "სამასი"
            g > 400 && g <= 499 -> return "ოთხას" + current
            g == 400 -> return "ოთხასი"
            g > 500 && g <= 599 -> return "ხუთას" + current
            g == 500 -> return "ხუთასი"
            g > 600 && g <= 699 -> return "ექვსას" + current
            g == 600 -> return "ექვსასი"
            g > 700 && g <= 799 -> return "შვიდას" + current
            g == 700 -> return "შვიდასი"
            g > 800 && g <= 899 -> return "რვაას" + current
            g == 800 -> return "რვაასი"
            g > 900 && g <= 999 -> return "ცხრაას" + current
            g == 900 -> return "ცხრაასი"
            else -> {
                return "ციფრი მეტია 1000-ზე"
            }
        }
    }
}